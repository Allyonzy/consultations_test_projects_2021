package car_builder;

public class Wheels {
    private WheelType wheelType; // тип шины
    private int rimProfileWidth; // ширина профиля обода
    private int landingDiameter; // посадочный диаметр
    private double landingShelvesInclinationAngle; // угол наклона посадочных полок

    public Wheels(WheelType wheelType) {
        this.wheelType = wheelType;
    }

    public Wheels(WheelType wheelType, int rimWidth, int landingDiameter, double landingShelvesInclinationAngle) {
        this.wheelType = wheelType;
        this.rimProfileWidth = rimWidth;
        this.landingDiameter = landingDiameter;
        this.landingShelvesInclinationAngle = landingShelvesInclinationAngle;
    }

    public WheelType getWheelType() {
        return wheelType;
    }

    @Override
    public String toString() {
        return "Колесо машины:\n" +
                "Тип колеса:" + wheelType +
                ", ширина профиля обода:" + rimProfileWidth +
                ", посадочный диаметр:" + landingDiameter +
                ", угол наклона посадочных полок:" + landingShelvesInclinationAngle;
    }
}
