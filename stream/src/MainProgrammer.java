package stream;

import java.util.*;
import java.util.stream.Collectors;

public class MainProgrammer {

    public static void main(String[] args) {
        testWithClass();

        System.exit(0);
    }

    // filter - возвращает stream, в котором есть только элементы, соответствующие условию фильтра
    // count - возвращает количество элементов в стриме
    // collect - преобразует stream в коллекцию или другую структуру данных
    // mapToInt - преобразовать объект в числовой стрим (стрим, содержащий числа)
    private static void testFilterAndCount() {
        System.out.println();
        System.out.println("Test filter and count start");

        Collection<String> collection =
                Arrays.asList("Поле", "Лампа", "Компьютер", "Паста", "Компьютер", "Чашка", "Чай", "Компьютер");

        Collection<Programmer> programmers = Arrays.asList(
                new Programmer("Вася", 20, Level.JUNIOR),
                new Programmer("Петя", 17, Level.JUNIOR),
                new Programmer("Катя", 25, Level.JUNIOR),
                new Programmer("Андреевич", 30, Level.SENIOR)
        );

        // Вернуть количество вхождений объекта
        long count = collection.stream().filter("Компьютер"::equals).count();
        System.out.println("count = " + count); // напечатает count = 2

        // Выбрать все элементы по шаблону
        List<String> select = collection
                .stream()
                .filter((s) -> s.contains("а"))
                .collect(Collectors.toList());
        System.out.println("select = " + select);

        // Выбрать вызванных в отдел кадров
        List<Programmer> entranceService = programmers
                .stream()
                .filter((p)-> p.getAge() >= 18 && p.getAge() < 27 || p.getLevel() == Level.JUNIOR)
                .collect(Collectors.toList());

        System.out.println("entranceService = " + entranceService);

        // Найти средний возраст среди джунов
        double juniorAverageAge = programmers
                .stream()
                .filter((p) -> p.getLevel() == Level.JUNIOR)
                .mapToInt(Programmer::getAge).average().getAsDouble();
        System.out.println("juniorAverageAge = " + juniorAverageAge);
    }

    // findFirst - возвращает первый Optional элемент из стрима
    // skip - пропускает N первых элементов (где N параметр метода)
    // collect преобразует stream в коллекцию или другую структуру данных
    private static void testFindFirstSkipCount() {
        Collection<String> collection = Arrays.asList("a1", "a2", "a3", "a1");

        System.out.println("Test findFirst and skip start");
        // вернуть первый элемент коллекции
        String first = collection.stream().findFirst().orElse("1");
        System.out.println("first = " + first); // напечатает first = a1

        // вернуть последний элемент коллекции
        String last = collection.stream().skip(collection.size() - 1).findAny().orElse("1");
        System.out.println("last = " + last ); // напечатает last = a1

        // найти элемент в коллекции
        String find = collection.stream().filter("a3"::equals).findFirst().get();
        System.out.println("find = " + find); // напечатает find = a3

        // вернуть третий элемент коллекции по порядку
        String third = collection.stream().skip(2).findFirst().get();
        System.out.println("third = " + third); // напечатает third = a3


        System.out.println();
        System.out.println("Test collect start");
        // выбрать все элементы по шаблону
        List<String> select = collection.stream().filter((s) -> s.contains("1")).collect(Collectors.toList());
        System.out.println("select = " + select); // напечатает select = [a1, a1]
    }

    // Метод Limit позволяет ограничить выборку определенным количеством первых элементов
    private static void testLimit() {
        System.out.println();
        System.out.println("Test limit start");

        HashSet<Programmer> collectionSet = new HashSet<>();
        collectionSet.add(new Programmer("Дима", 19, Level.TRAINEE));
        collectionSet.add(new Programmer("Петя", 17, Level.JUNIOR));
        collectionSet.add(new Programmer("Вася", 20, Level.JUNIOR));
        collectionSet.add(new Programmer("Катя", 25, Level.JUNIOR));
        collectionSet.add(new Programmer("Шурик", 35, Level.MIDDLE));
        collectionSet.add(new Programmer("Максим", 34, Level.MIDDLE));
        collectionSet.add(new Programmer("Витя", 29, Level.MIDDLE));
        collectionSet.add(new Programmer("Андреевич", 30, Level.SENIOR));

        LinkedHashSet<Programmer> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.add(new Programmer("Дима", 16, Level.TRAINEE));
        linkedHashSet.add(new Programmer("Петя", 22, Level.JUNIOR));
        linkedHashSet.add(new Programmer("Вася", 43, Level.JUNIOR));
        linkedHashSet.add(new Programmer("Катя", 23, Level.JUNIOR));
        linkedHashSet.add(new Programmer("Шурик", 45, Level.MIDDLE));
        linkedHashSet.add(new Programmer("Максим", 23, Level.MIDDLE));
        linkedHashSet.add(new Programmer("Витя", 21, Level.MIDDLE));
        linkedHashSet.add(new Programmer("Андреевич", 40, Level.SENIOR));

        // Вернуть первые три элемента
        List<Programmer> limitCollectionSet = collectionSet.stream().limit(3).collect(Collectors.toList());
        System.out.println("limit = " + limitCollectionSet);
        List<Programmer> limitLinkedHashSet = linkedHashSet.stream().limit(3).collect(Collectors.toList());
        System.out.println("limitLinkedHashSet = " + limitLinkedHashSet);

        List<Programmer> fromToCollectionSet = collectionSet.stream().skip(1).limit(2).collect(Collectors.toList());
        System.out.println("limit = " + fromToCollectionSet);
        List<Programmer> fromToLinkedHashSet = linkedHashSet.stream().skip(1).limit(2).collect(Collectors.toList());
        System.out.println("limitLinkedHashSet = " + fromToLinkedHashSet);

//        Programmer emptyProgrammer = new Programmer("NoName", 0, Level.TRAINEE);

        Programmer lastCollectionSet = collectionSet
                .stream()
                .skip(collectionSet.size() - 1)
                .findAny()
                .get();

        System.out.println("limit = " + lastCollectionSet);
        Programmer lastLinkedHashSet = linkedHashSet
                .stream()
                .skip(linkedHashSet.size() - 1)
                .findAny()
                .get();
        System.out.println("limitLinkedHashSet = " + lastLinkedHashSet);

    }

    // Метод distinct возвращает stream без дубликатов,
    // при этом для упорядоченного стрима (например, коллекция на основе list) порядок стабилен ,
    // для неупорядоченного - порядок не гарантируется
    // Метод collect преобразует stream в коллекцию или другую структуру данных
    private static void testDistinct() {
        System.out.println();
        System.out.println("Test distinct start");

        List<Programmer> collectionOrderedList = new ArrayList<>();
        collectionOrderedList.add(new Programmer("Питер", 19, Level.TRAINEE));
        collectionOrderedList.add(new Programmer("Жанна", 17, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Жанна", 17, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Жанна", 17, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Вика", 20, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Лёша", 25, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Лёша", 25, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Лёша", 25, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Лёша", 25, Level.JUNIOR));
        collectionOrderedList.add(new Programmer("Александр", 35, Level.MIDDLE));
        collectionOrderedList.add(new Programmer("Саша", 34, Level.MIDDLE));
        collectionOrderedList.add(new Programmer("Саша", 34, Level.MIDDLE));
        collectionOrderedList.add(new Programmer("Саша", 34, Level.MIDDLE));
        collectionOrderedList.add(new Programmer("Саша", 34, Level.MIDDLE));
        collectionOrderedList.add(new Programmer("Шурик", 29, Level.MIDDLE));
        collectionOrderedList.add(new Programmer("Сергей", 30, Level.SENIOR));

        Set<Programmer> nonOrderedCollection = new HashSet<>();
        nonOrderedCollection.add(new Programmer("Дима", 16, Level.JUNIOR));
        nonOrderedCollection.add(new Programmer("Петя", 22, Level.TRAINEE));
        nonOrderedCollection.add(new Programmer("Петя", 22, Level.TRAINEE));
        nonOrderedCollection.add(new Programmer("Петя", 22, Level.TRAINEE));
        nonOrderedCollection.add(new Programmer("Петя", 22, Level.TRAINEE));
        nonOrderedCollection.add(new Programmer("Вася", 43, Level.TRAINEE));
        nonOrderedCollection.add(new Programmer("Катя", 23, Level.TRAINEE));
        nonOrderedCollection.add(new Programmer("Шурик", 45, Level.TRAINEE));
        nonOrderedCollection.add(new Programmer("Максим", 23, Level.SENIOR));
        nonOrderedCollection.add(new Programmer("Максим", 23, Level.SENIOR));
        nonOrderedCollection.add(new Programmer("Максим", 23, Level.SENIOR));
        nonOrderedCollection.add(new Programmer("Максим", 23, Level.SENIOR));
        nonOrderedCollection.add(new Programmer("Витя", 21, Level.SENIOR));
        nonOrderedCollection.add(new Programmer("Андреевич", 40, Level.JUNIOR));
        nonOrderedCollection.add(new Programmer("Андреевич", 40, Level.JUNIOR));
        nonOrderedCollection.add(new Programmer("Андреевич", 40, Level.JUNIOR));
        nonOrderedCollection.add(new Programmer("Андреевич", 40, Level.JUNIOR));

        // Получение коллекции без дубликатов
        List<Programmer> distinct = collectionOrderedList
                .stream()
                .distinct()
                .collect(Collectors.toList());

        System.out.println("distinct = " + distinct); // напечатает distinct = [a1, a2, a3] - порядок не гарантируется

        List<Programmer> distinctOrdered = nonOrderedCollection
                .stream()
                .distinct()
                .collect(Collectors.toList());

        System.out.println("distinctOrdered = " + distinctOrdered); // напечатает distinct = [a1, a2, a3] - порядок гарантируется
    }

    // Метод Map изменяет выборку по определенному правилу, возвращает stream с новой выборкой
    private static void testMap() {
        System.out.println();
        System.out.println("Test map start");

        List<Programmer> programmers = new ArrayList<>();
        programmers.add(new Programmer("Дима", 17, Level.TRAINEE));
        programmers.add(new Programmer("Петя", 23, Level.JUNIOR));
        programmers.add(new Programmer("Вася", 42, Level.JUNIOR));
        programmers.add(new Programmer("Катя", 21, Level.JUNIOR));
        programmers.add(new Programmer("Шурик", 43, Level.MIDDLE));
        programmers.add(new Programmer("Максим", 22, Level.MIDDLE));
        programmers.add(new Programmer("Витя", 20, Level.MIDDLE));
        programmers.add(new Programmer("Андреевич", 42, Level.SENIOR));

        // Изменение всех элементов коллекции
        List<String> transform = programmers
                .stream()
                .map((item) -> item.getName() + ": " + item.getLevel())
                .collect(Collectors.toList());

        System.out.println("transform = " + transform); // напечатает transform = [a1_1, a2_1, a3_1, a1_1]

        // убрать первый символ и вернуть числа
        List<Integer> number = programmers
                .stream()
                .map((item) -> item.getAge())
                .collect(Collectors.toList());
        System.out.println("number = " + number); // напечатает transform = [1, 2, 3, 1]
    }

    // Метод MapToInt - изменяет выборку по определенному правилу, возвращает stream с новой числовой выборкой
    private static void testMapToInt() {
        System.out.println();
        System.out.println("Test mapToInt start");
        Collection<String> collection = Arrays.asList("a1", "a2", "a3", "a1");
        // убрать первый символ и вернуть числа
        int[] number = collection.stream().mapToInt((s) -> Integer.parseInt(s.substring(1))).toArray();
        System.out.println("number = " + Arrays.toString(number)); // напечатает number = [1, 2, 3, 1]

    }

    // Метод FlatMap - похоже на Map - только вместо одного значения, он возвращает целый stream значений
    private static void testFlatMap() {
        System.out.println();
        System.out.println("Test flat map start");
        Collection<String> collection = Arrays.asList("1,2,0", "4,5");
        // получить все числовые значения, которые хранятся через запятую в collection
        String[] number = collection
                .stream()
                .flatMap((p) -> Arrays.asList(p.split(",")).stream()).toArray(String[]::new);
        System.out.println("number = " + Arrays.toString(number)); // напечатает number = [1, 2, 0, 4, 5]
    }

    // Метод FlatMapToInt - похоже на MapToInt - только вместо одного значения, он возвращает целый stream значений
    private static void testFlatMapToInt() {
        System.out.println();
        System.out.println("Test flat map start");
        Collection<String> collection = Arrays.asList("1,2,0", "4,5", "7,8,9", "67,3,4,7");
        // получить сумму всех числовые значения, которые хранятся через запятую в collection
        int sum = collection
                .stream().flatMapToInt((p) -> Arrays.asList(p.split(","))
                .stream().mapToInt(Integer::parseInt)).sum();
        System.out.println("sum = " + sum); // напечатает sum = 12
    }

    private static void testWithClass() {
        Map<String,Programmer> staffMap = new HashMap();
        staffMap.put("144-25-5464", new Programmer("Amy Lee", 34, Level.MIDDLE, 2));
        staffMap.put("567-24-2546", new Programmer("Harry Hacker", 32, Level.SENIOR, 4));
        staffMap.put("157-62-7935", new Programmer("Gary Cooper", 17, Level.TRAINEE, 1));
        staffMap.put("456-62-5527", new Programmer("Francesca Cruz", 22, Level.SENIOR, 4));

        int sum = staffMap
                .values()
                .stream()
                .map(Programmer::getProgrammerLanguagesNumber)
                .mapToInt(Integer::intValue).sum();

        System.out.println("sum = " + sum); // напечатает sum = 12

    }
}
