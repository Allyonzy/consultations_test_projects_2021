import model.*;
import repository.*;

import java.util.*;
import java.util.stream.Collectors;

public class MainDemo {

    public static void main(String[] args) {
        // dump файлы (загрузим информацию из txt)
        ReaderRepository readerRepository = new ReaderRepositoryImpl();
        BookRepositoryImpl bookRepository = new BookRepositoryImpl();
        AuthorRepository authorRepository = new AuthorRepositoryImpl();

        List<Reader> readers = readerRepository.findAll();
        List<Book> books = bookRepository.findAll();
        List<Author> authors = authorRepository.findAll();

        Library library = new Library(books, readers);

        List<Book> sortedBooks = library
                .getBooks()
                .stream()
                .sorted(Comparator.comparing(Book::getIssueYear))
                .collect(Collectors.toList());

        List<String> filteredBooks = library
                .getBooks()
                .stream()
                .filter(book -> book.getIssueYear() == 2021)
                .map(Book::getName)
                .collect(Collectors.toList());

        List<EmailAddress> addresses = library
                .getReaders()
                .stream()
                .filter(reader -> reader.isSubscriber())
                .map(Reader::getEmail)
                .map(EmailAddress::new)
                .collect(Collectors.toList());

        System.exit(0);

    }
}
