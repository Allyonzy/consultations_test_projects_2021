package stream;

import java.util.Objects;

public class Programmer {
    private String name;
    private Integer age;
    private Level level;
    private Integer programmerLanguagesNumber = 0;

    public Programmer() {}

    public Programmer(String name, Integer age, Level level) {
        this.name = name;
        this.age = age;
        this.level = level;
    }

    public Programmer(String name, Integer age, Level level, Integer programmerLanguagesNumber) {
        this.name = name;
        this.age = age;
        this.level = level;
        this.programmerLanguagesNumber = programmerLanguagesNumber;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public Level getLevel() {
        return level;
    }

    public Integer getProgrammerLanguagesNumber() {
        return programmerLanguagesNumber;
    }

    public void setProgrammerLanguagesNumber(Integer programmerLanguagesNumber) {
        this.programmerLanguagesNumber = programmerLanguagesNumber;
    }

    @Override
    public String toString() {
        return "Programmer: {" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", level=" + level +
                "}\n\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Programmer)) return false;
        Programmer programmer = (Programmer) o;
        return Objects.equals(name, programmer.getName()) &&
                Objects.equals(age, programmer.getAge()) &&
                Objects.equals(level, programmer.getLevel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, level);
    }
}
