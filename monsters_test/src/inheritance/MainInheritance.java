package inheritance;

import java.util.ArrayList;
import java.util.List;

public class MainInheritance {
    public static void main(String[] args) {
        String[] powersBanshi = new String[] {"Killing voice"};
        Banshi banshi = new Banshi("Some banshi", powersBanshi);

        String[] powersVampire = new String[] {"Killing teeth"};
        Vampire vampire = new Vampire("Some vampire", powersVampire);

        String[] powersMavki = new String[] {"Killing teeth", "Artefact"};
        Mavka mavka = new Mavka("Some Mavka", powersMavki);

        List<MonsterRoar> monsterRoars = new ArrayList<>();
        monsterRoars.add(banshi);
        monsterRoars.add(vampire);
        monsterRoars.add(mavka);

        monsterRoars.forEach( monsterHere -> {
            monsterHere.entranceRoar();
        });

        MonsterRoar[] monsterRoarsArray = new MonsterRoar[3];
        monsterRoarsArray[0] = banshi;
        monsterRoarsArray[1] = vampire;
        monsterRoarsArray[2] = mavka;

        for(MonsterRoar monsterHere : monsterRoarsArray) {
            monsterHere.entranceRoar();
        }

        System.exit(0);
    }

    public static void sortBanshi(Banshi[] banshis) {
        // пройдемся по массиву
        for (int i = 0; i < banshis.length; i++) {
            // запишем первый элемент как минимальное значение
            double min = banshis[i].getHealth();
            // запишем индекс элемента как индекс с минимальным значением
            int minIndex = i;
            for (int j = i + 1; j < banshis.length; j++) {
                // сравним с соседними элементами массива
                if (banshis[j].getHealth() < min) {
                    min = banshis[j].getHealth();
                    minIndex = j;
                }
            }
//            LinkedList

            // Переместим объект на другую позицию
            if (i != minIndex) {
                Banshi banshi = banshis[i];
                banshis[i] = banshis[minIndex];
                banshis[minIndex] = banshi;
            }
        }
    }
}
