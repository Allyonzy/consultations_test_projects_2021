package files;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class MainReadFile {
    public static void main(String[] args) throws IOException {

        String fileName = "./source.txt";

        //Используем класс Files для обработки небольших файлов, получаем содержимое файла файла
        readUsingFiles(fileName);

        // используем класс Scanner для больших файлов, читаем построчно
        readUsingScanner(fileName);

        // построчно читаем с файла с помощью BufferedReader
        readUsingBufferedReader(fileName);
        Charset charset = StandardCharsets.UTF_8;
        readUsingBufferedReaderJava7(fileName, charset);
        readUsingBufferedReader(fileName, charset);

        // читаем с помощью FileReader без поддержки кодировки
        readUsingFileReader(fileName);
    }

    private static void readUsingFileReader(String fileName) throws IOException {
        File file = new File(fileName);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine()) != null){
            //обрабатываем считанную строку - пишем ее в консоль
            System.out.println(line);
        }
        br.close();
        fr.close();

    }

    private static void readUsingBufferedReader(String fileName, Charset cs) throws IOException {
        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, cs);
        BufferedReader br = new BufferedReader(isr);
        String line;
        while((line = br.readLine()) != null){
            System.out.println(line);
        }
        br.close();

    }

    private static void readUsingBufferedReaderJava7(String fileName, Charset cs) throws IOException {
        Path path = Paths.get(fileName);
        BufferedReader br = Files.newBufferedReader(path, cs);
        String line;
        while((line = br.readLine()) != null){
            System.out.println(line);
        }
        br.close();
    }

    private static void readUsingBufferedReader(String fileName) throws IOException {
        File file = new File(fileName);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine()) != null){
            System.out.println(line);
        }
        br.close();
        fr.close();
    }

    private static void readUsingScanner(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        Scanner scanner = new Scanner(path);
        //читаем построчно
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
        }
    }

    private static void readUsingFiles(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        //считываем содержимое файла в массив байт
        byte[] bytes = Files.readAllBytes(path);
        //считываем содержимое файла в список строк
        List<String> allLines = Files.readAllLines(path, StandardCharsets.UTF_8);
    }

    /**
     * Итерировать через все строки в файле –
     * позволяет для обработки каждой строки – без хранения ссылок на них
     * @param fileName
     * @throws IOException
     */
    private static void readAllStrings(String fileName) throws IOException {
        FileInputStream inputStream = null;
        Scanner sc = null;
        Path path = Paths.get(fileName);
        try {
            inputStream = new FileInputStream(path.toString());
            sc = new Scanner(inputStream, "UTF-8");
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                // System.out.println(line);
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }
    }


}
