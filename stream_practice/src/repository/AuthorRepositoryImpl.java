package repository;

import lib.FileController;
import model.Author;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AuthorRepositoryImpl implements AuthorRepository, FileController {

    public static final String FILE_NAME = "stream_practice/src/data/input_author.txt";

    public List<Author> findAll() {
        List<String> data = readFile(FILE_NAME);

        List<Author> authors = new ArrayList<>();
        data.forEach(line -> {
            String[] parts = line.split("\\|");
            List<String> interestsList = Arrays.asList(parts[2].split(" ,"));
            authors.add(new Author(parts[0], parts[1], interestsList));
        });

        return authors;
    }

    @Override
    public void save(Author book) {

    }

    @Override
    public List<Author> findByInterest(String interest) {
        return null;
    }

    @Override
    public List<Author> findBySurname(String surname) {
        return null;
    }
}
