package car_builder;

/**
 * Руководство автомобиля — это второй продукт. Заметьте, что руководство и сам
 * автомобиль не имеют общего родительского класса. По сути, они независимы.
 */
public class Manual {
    private final CarType carType;
    private final int seats;
    private final Engine engine;
    private final Transmission transmission;
    private final TripComputer tripComputer;
    private final GPSNavigator gpsNavigator;
    private final Wheels[] carWheels;

    public Manual(CarType carType, int seats, Engine engine, Transmission transmission,
                  TripComputer tripComputer, GPSNavigator gpsNavigator, Wheels[] carWheels) {
        this.carType = carType;
        this.seats = seats;
        this.engine = engine;
        this.transmission = transmission;
        this.tripComputer = tripComputer;
        this.gpsNavigator = gpsNavigator;
        this.carWheels = carWheels;
    }

    public String print() {
        String info = "";
        info += "Тип машины: " + carType + "\n";
        info += "Число сидений: " + seats + "\n";
        info += "Двигатель: объем - " + engine.getVolume() + "; пробег - " + engine.getMileage() + "\n";
        info += "Трансмиссия: " + transmission + "\n";
        if (this.tripComputer != null) {
            info += "Бортовой компьютер: Функционал" + "\n";
        } else {
            info += "Бортовой компьютер: N/A" + "\n";
        }
        if (this.gpsNavigator != null) {
            info += "GPS навигатор: Функционал" + "\n";
        } else {
            info += "GPS навигатор: N/A" + "\n";
        }
        info += "Колёса машины" + "\n";
        for(int i = 0; i < carWheels.length; i++) {
            info += (i + 1) + ".\n" + carWheels[i].toString() + "\n";
        }

        return info;
    }
}
