package car_builder;

/**
 * Инженер по автоматизации знает в какой последовательности заставлять работать строителя. Он
 * работает с ним через общий интерфейс Строителя. Из-за этого, он может не
 * знать какой конкретно продукт сейчас строится.
 */
public class AutomationEngineer {
    public void constructSportsCar(Builder builder) {
        builder.setCarType(CarType.SPORTS_CAR);
        builder.setSeats(2);
        builder.setEngine(new Engine(3.0, 0));
        builder.setTransmission(Transmission.SEMI_AUTOMATIC);
        builder.setTripComputer(new TripComputer());
        builder.setGPSNavigator(new GPSNavigator());
        builder.setCarWheels(new Wheels[] {
                new Wheels(WheelType.SUMMER_TIRED),
                new Wheels(WheelType.SUMMER_TIRED),
                new Wheels(WheelType.SUMMER_TIRED),
                new Wheels(WheelType.SUMMER_TIRED)
        });
    }

    public void constructCityCar(Builder builder) {
        builder.setCarType(CarType.CITY_CAR);
        builder.setSeats(2);
        builder.setEngine(new Engine(1.2, 0));
        builder.setTransmission(Transmission.AUTOMATIC);
        builder.setTripComputer(new TripComputer());
        builder.setGPSNavigator(new GPSNavigator());
        builder.setCarWheels(new Wheels[] {
                new Wheels(WheelType.SUMMER_TIRED),
                new Wheels(WheelType.SUMMER_TIRED),
                new Wheels(WheelType.SUMMER_TIRED),
                new Wheels(WheelType.SUMMER_TIRED)
        });
    }

    public void constructSUV(Builder builder) {
        builder.setCarType(CarType.SUV);
        builder.setSeats(4);
        builder.setEngine(new Engine(2.5, 0));
        builder.setTransmission(Transmission.MANUAL);
        builder.setGPSNavigator(new GPSNavigator());
        builder.setCarWheels(new Wheels[] {
                new Wheels(WheelType.STUDDED),
                new Wheels(WheelType.STUDDED),
                new Wheels(WheelType.STUDDED),
                new Wheels(WheelType.STUDDED)
        });
    }
}
