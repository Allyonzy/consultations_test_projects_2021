package repository;

import model.Author;

import java.util.List;

public interface AuthorRepository {
    List<Author> findAll();
    void save(Author book);
    List<Author> findByInterest(String interest);
    List<Author> findBySurname(String surname);
}
