package inheritance;

public class Vampire extends Monster implements MonsterRoar {

    private static final String TYPE = "VAMPIRE";

    public Vampire(String name, String[] powers) {
        super(name, powers);
    }

    @Override
    public void entrance() {
        System.out.println(TYPE + super.getName() + "  вышел на охоту");
    }

    @Override
    public void entranceRoar() {
        System.out.println("Hrrrrr... Some blood comes to me!");
    }
}
