package repository;

import model.Author;
import model.Book;

import java.util.List;

public interface BookRepository {
    List<Book> findAll();
    void save(Book book);
    List<Book> findByAuthor(Author author);
    List<Book> findByName(String name);
}
