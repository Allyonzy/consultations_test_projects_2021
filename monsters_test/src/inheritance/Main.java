package inheritance;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {
    public static final String FILE_NAME = "monsters_test/src/inheritance/input_files/banshi.txt";
    public static final String FILE_NAME_OUTPUT = "monsters_test/src/inheritance/output_files/monster.txt";
    public static final String FILE_NAME_ERROR = "banshi.txt";

    public static void main(String[] args) {
        Monster[] monsters = testObjectCreation();

//        save(monsters);
        testReadingBanshi().forEach(System.out::println);

        System.exit(0);
    }



    public static List<Banshi> testReadingBanshi() {
        List<Banshi> banshes = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME))) {
            return reader
                    .lines()
                    .map(BanshiRepository.banshiMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
//            throw new IllegalArgumentException(e);
            System.err.println(e);
            return banshes;
        }
    }

    public static void save(Monster[] monsters) {
        try(
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE_NAME_OUTPUT, true));
        ) {
            for (Monster monster: monsters) {
                bufferedWriter.write(monster.getName() + "|" + monster.getPowersAsString());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        } catch (IOException e) {
//            throw new IllegalArgumentException(e);
            System.err.println(e);
        }
    }

    public static Monster[] testObjectCreation() {

        String[] powersBanshi = new String[] {"Killing voice"};
        Banshi banshi = new Banshi("Some banshi", powersBanshi);

        String[] powersVampire = new String[] {"Killing teeth"};
        Vampire vampire = new Vampire("Some vampire", powersVampire);

        String[] powersMavki = new String[] {"Killing teeth", "Artefact"};
        Mavka mavka = new Mavka("Some Mavka", powersMavki);

        Monster monster = banshi;

        Object obj = vampire;
        vampire = (Vampire)obj; // нисходящее преобразование
        MonsterRoar[] monsterRoars = new MonsterRoar[] {vampire, banshi};

        for (MonsterRoar monsterRoar : monsterRoars) {
            monsterRoar.entranceRoar();
        }

        Monster[] monsters = {vampire, banshi, mavka, monster};

        return monsters;
    }

    public static void sortBanshi(Banshi[] banshis) {
        // пройдемся по массиву
        for (int i = 0; i < banshis.length; i++) {
            // запишем первый элемент как минимальное значение
            double min = banshis[i].getHealth();
            // запишем индекс элемента как индекс с минимальным значением
            int minIndex = i;
            for (int j = i + 1; j < banshis.length; j++) {
                // сравним с соседними элементами массива
                if (banshis[j].getHealth() < min) {
                    min = banshis[j].getHealth();
                    minIndex = j;
                }
            }
//            LinkedList

            // Переместим объект на другую позицию
            if (i != minIndex) {
                Banshi banshi = banshis[i];
                banshis[i] = banshis[minIndex];
                banshis[minIndex] = banshi;
            }
        }
    }
}
