package repository;

import lib.FileController;
import model.Reader;

import java.util.ArrayList;
import java.util.List;

public class ReaderRepositoryImpl implements ReaderRepository, FileController {

    public static final String FILE_NAME = "stream_practice/src/data/input_reader.txt";

    @Override
    public List<Reader> findAll() {
        List<String> data = readFile(FILE_NAME);
        List<Reader> readers = new ArrayList<>();

        data.forEach(line -> {
            String[] parts = line.split("\\|");
            readers.add(new Reader(parts[0], parts[1], Boolean.parseBoolean(parts[2])));
        });

        return readers;
    }

    @Override
    public void save(Reader reader) {

    }

}
