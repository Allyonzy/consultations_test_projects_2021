package files;

import java.util.Date;

public class MainFormat {
    public static void testFormat() {
        String s1 = "Hello,";
        String s2 = "brave";
        String s3 = "new";
        String s4 = "world";
        // Выводим в одну строку
        System.out.printf("%s %s %s %s\n", s1,s2,s3,s4);
        System.out.println("************************");
        // Выводим в несколько строк
        System.out.printf("%s\n%s\n%s\n%s\n", s1,s2,s3,s4);
        System.out.println("************************");

        int number = 123;
        // Просто выводим число - %d
        // Символ n - для перехода на следующую строку
        System.out.println("Просто выводим число ");
        System.out.printf("%d\n", number);

        // выводим число и указываем ширину строки - 8 символов
        System.out.println("Выводим число и указываем ширину строки - 8 символов");
        System.out.printf("%8d\n", number);

        // выводим число и указываем ширину строки - 8 символов
        // начало строки забиваем нулями
        System.out.println("Начало строки забиваем нулями");
        System.out.printf("%08d\n", number);
        System.out.println("************************");

        // Выводим символ - %c
        System.out.println("Выводим символ");
        System.out.printf("%c\n", s1.charAt(0));

        // Выводим символ - при помощи указания кода символа
        // символ H имеет код 72
        System.out.println("Символ с кодом 72 это ");
        System.out.printf("%c\n", 72);

        // Выводим символ - и указываем ширину строки - 8 символов
        System.out.println("Выводим символ - и указываем ширину строки - 8 символов");
        System.out.printf("%8c\n", s1.charAt(0));
        System.out.println("************************");

        // Выводим булевое значение - true или false
        System.out.println("Выводим булевое значение 5<6 ");
        System.out.printf("%b\n", 5 < 6);

        // Выводим булевое значение - true или false
        //и задаем ширину строки 8 символов
        System.out.println("Выводим булевое значение и задаем ширину строки 8 символов");
        System.out.printf("%8b\n", 6 < 5);

        System.out.println("************************");
        Date today = new Date();
        // Какой сегодня день месяца
        System.out.printf("Сегодня день месяца %td\n", today);

        // Какой сегодня месяц в году
        System.out.printf("Сегодня месяц в году %tm\n", today);

        // Какой сегодня год
        System.out.printf("Сегодня год (последние две цифры года) %ty\n", today);

        // Какая сегодня дата
        System.out.printf("Сегодня дата в формате dd/MM/yy %tD\n", today);

        // Какой сегодня год полностью
        System.out.printf("Сегодня год полностью %tY\n", today);

        // Текущее время - час-минута-секунда
        System.out.printf("Текущее время в формате hh/mm/ss %tT\n", today);

        // Текущяя дата - год-месяц-день
        System.out.printf("Текущая дата в формате YYYY-MM-dd %tF\n", today);

        // Текущий день месяца
        System.out.printf("Текущий день месяца %te\n", today);

        // Текущий месяц прописью
        System.out.printf("Текущий месяц прописью %tB\n", today);

        System.out.println("************************");
        // Вывод числа со знаком впереди
        System.out.printf("Число с плюслм впереди +%d\n", 1234);
        System.out.printf("Число с минусом впереди -%d\n", 4321);
        System.out.println("************************");

        // Десятичное число с точкой - %f
        System.out.println("Десятичное число с точкой");
        System.out.printf("%f\n", Math.PI);

        // Десятичное число с экспонентой - %e
        System.out.println("Десятичное число с экспонентой ");
        System.out.printf("%e\n", Math.PI);

        // Десятичное число с точкой - %f
        // устанавливаем количество знаков после запятой - 4
        System.out.println("Десятичное число с точкой и 4 знака после точки ");
        System.out.printf("%.4f\n", Math.PI);

        // устанавливаем количество знаков после запятой - 8
        System.out.println("Десятичное число с точкой и 8 знаков после точки ");
        System.out.printf("%.8f\n", Math.PI);

        // устанавливаем количество знаков после запятой - 4
        //  общее количество символов - 8
        System.out.println("Всего 8 знаков и 4 знако после точки ");
        System.out.printf("%8.4f\n", Math.PI);

        // Устанавливаем разделитель
        System.out.println("Устанавливаем разделитель между тысячами в целом числе");
        System.out.printf("%,d\n", 100000000);

        // Устанавливаем разделитель - знак запятой ','
        // но в русской локале запятая - это разделитель
        // по умолчанию - поэтому ничего не изменилось
        System.out.println("Десятичное число с точкой всего 6 знакво и 2 после точки ");
        System.out.printf( "%,6.2f", Math.PI);

    }

}
