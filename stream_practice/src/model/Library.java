package model;

import java.util.List;
import java.util.UUID;

public class Library {
    private UUID libraryId = UUID.randomUUID(); //Идентификатор
    private List<Book> books;
    private List<Reader> readers;

    public Library() {}

    public Library(List<Book> books, List<Reader> readers) {
        this.books = books;
        this.readers = readers;
    }



    public List<Book> getBooks() {
        return books;
    }

    public List<Reader> getReaders() {
        return readers;
    }
}