package model;

import java.util.List;
import java.util.UUID;

public class Author {
    private UUID authorId = UUID.randomUUID();; // идентификатор
    private String name; // имя-отчество
    private String surname; // фамилия
    private Integer[] century; // в каком веке писал
    private String country; // в каком веке писал
    private List<String> interests; // жанр

    public Author() {}

    public Author(String surname) {
        this.surname = surname;
    }

    public Author(String name, String surname, Integer[] century, String country) {
        this.name = name;
        this.surname = surname;
        this.century = century;
        this.country = country;
    }

    public Author(String name, String surname, List<String> interests) {
        this.name = name;
        this.surname = surname;
        this.interests = interests;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Integer[] getCentury() {
        return century;
    }

    public String getCountry() {
        return country;
    }
}
