package api;

import domen.Player;

import java.util.*;

public class PlayerDAO implements DAO<Player> {

    private List<Player> players = new ArrayList<>();

    @Override
    public Optional<Player> get(long id) {
//        UUID uuid = UUID.randomUUID();
        return Optional.ofNullable(players.get((int) id));
    }

    @Override
    public List<Player> getAll() {
        return players;
    }

    @Override
    public void save(Player player) {
        players.add(player);
    }

    @Override
    public void update(Player player, String[] params) {
        player.setNickname(Objects.requireNonNull(
                params[0], "Name cannot be null"));
        player.setEmail(Objects.requireNonNull(
                params[1], "Email cannot be null"));

        players.add(player);
    }

    @Override
    public void delete(Player Player) {
        players.remove(Player);
    }
}