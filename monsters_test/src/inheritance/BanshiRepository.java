package inheritance;

import java.util.function.Function;

public abstract class BanshiRepository {
    public static final Function<String, Banshi> banshiMapper = line -> {
        String[] parseLine = line.split("\\|");
        String name = parseLine[0];
        String[] powers = parseLine[1].split(", ");
        int health = Integer.parseInt(parseLine[2]);
        int loudVoicePower = Integer.parseInt(parseLine[3]);

        return new Banshi(name, powers, health, loudVoicePower);
    };
}
