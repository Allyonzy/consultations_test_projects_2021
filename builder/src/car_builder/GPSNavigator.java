package car_builder;

public class GPSNavigator {
    private String route;

    public GPSNavigator() {
        this.route = "Республика Татарстан, Верхнеуслонский район, город Иннополис, Спортивная улица, дом 107";
    }

    public GPSNavigator(String manualRoute) {
        this.route = manualRoute;
    }

    public String getRoute() {
        return route;
    }
}
