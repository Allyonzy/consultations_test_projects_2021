package inheritance;

public class Mavka extends Vampire {

    private static final String TYPE = "MAVKA";

    public Mavka(String name, String[] powers) {
        super(name, powers);
    }

    @Override
    public void entrance() {
        System.out.println(TYPE + super.getName() + "  вышел на охоту");
    }
}
