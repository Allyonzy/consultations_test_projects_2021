package files;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        testPathFiles();
        System.exit(0);
    }

    public static void testFile() {
        try {
            File fileTest = new File("./../input_files/../InputDemo.txt");
            printPaths(fileTest);
            File file = new File("D:\\Courses2021\\JavaJunior\\pcs_java_21_02\\OutputDemo.txt");
            printPaths(file);
            file = new File("D:/Courses2021/JavaJunior/pcs_java_21_02/OutputDemo.txt");
            printPaths(file);
            // relative path
            file = new File("OutputDemo.txt");
            printPaths(file);
            // complex relative paths
            file = new File("D:/Courses2021/JavaJunior/pcs_java_21_02/../InputDemo.txt");
            printPaths(file);
            // URI paths
            file = new File(new URI("file:///pcs_java_21_02/../OutputDemo2.txt"));
            printPaths(file);
        } catch(IOException e) {
            System.err.println(e);
        } catch(URISyntaxException e) {
            System.err.println(e);
        }
    }

    private static void printPaths(File file) throws IOException {
        System.out.println("Absolute Path: " + file.getAbsolutePath());
        System.out.println("Canonical Path: " + file.getCanonicalPath());
        System.out.println("Path: " + file.getPath());
        System.out.println();
    }

    public static void testPath() {
        String str = "D:\\Courses2021\\JavaJunior\\pcs_java_21_02\\OutputDemo.txt";
        Path testFilePath = Paths.get(str);
        Path path = FileSystems.getDefault().getPath(str);
        try(
            BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
        ) {

        } catch(IOException e) {
            System.err.println(e);
        }


    }

    public static void testPathFiles() {
        Path testFilePath = Paths.get("D:\\Courses2021\\JavaJunior\\pcs_java_21_02\\OutputDemo.txt");

        Path fileName = testFilePath.getFileName();
        System.out.println(fileName);

        Path parent = testFilePath.getParent();
        System.out.println(parent);

        Path root = testFilePath.getRoot();
        System.out.println(root);

        boolean endWithTxt = root.endsWith("/OutputDemo.txt");
        System.out.println(endWithTxt);

        boolean endsWithFileName = parent.endsWith("/../InputDemo.txt");
        System.out.println(endsWithFileName);
    }
}
