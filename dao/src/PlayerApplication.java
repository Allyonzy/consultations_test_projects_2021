import api.DAO;
import api.PlayerDAO;
import domen.Player;

import java.util.Optional;

public class PlayerApplication {
    private static DAO playerDAO;

    public static void main(String[] args) {
        playerDAO = new PlayerDAO();

        Player player = getPlayer(0);
        System.out.println(player);
        playerDAO.update(player, new String[]{"Jake", "[email protected]"});

        Player nextPlayer = getPlayer(1);
        playerDAO.delete(nextPlayer);
        playerDAO.save(new Player("Julie", "[email protected]"));

        playerDAO.getAll().stream().map(obj -> {
            Player currentPlayer = (Player)obj;
            return currentPlayer.getNickname();
        });
    }

    private static Player getPlayer(long id) {
        Optional<Player> player = playerDAO.get(id);

        return player.orElseGet(
                () -> new Player("non-existing user", "no-email"));
    }
}
