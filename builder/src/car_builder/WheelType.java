package car_builder;

public enum WheelType {
    SUMMER_TIRED, // летняя резина
    STUDDED // шипованная резина
}
