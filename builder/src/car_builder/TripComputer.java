package car_builder;

public class TripComputer {
    private Car car;

    public void setCar(Car car) {
        this.car = car;
    }

    public void showFuelLevel() {
        System.out.println("Уровень заполнения бака: " + car.getFuel());
    }

    public void showStatus() {
        if (this.car.getEngine().isStarted()) {
            System.out.println("Двигатель работает, машина поехала");
        } else {
            System.out.println("Машина стоит на месте");
        }
    }
}
