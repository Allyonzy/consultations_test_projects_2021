package inheritance;

import java.util.Arrays;

public class Monster {

    private String name;
    private String[] powers;

    public Monster(String name, String[] powers) {
        this.name = name;
        this.powers = powers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getPowers() {
        return powers;
    }

    public String getPowersAsString() {
        return String.join(", ", this.powers);
    }

    public void setPowers(String[] powers) {
        this.powers = powers;
    }

    public void entrance() {
        System.out.println(this.name + " вышел на охоту");
    }

    @Override
    public String toString() {
        return "Monster{" +
                "name='" + name + '\'' +
                ", powers=" + Arrays.toString(powers) +
                '}';
    }
}
