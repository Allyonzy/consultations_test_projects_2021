package repository;

import lib.FileController;
import model.Author;
import model.Book;

import java.util.ArrayList;
import java.util.List;

public class BookRepositoryImpl implements BookRepository, FileController {

    public static final String FILE_NAME = "stream_practice/src/data/input_book.txt";

    @Override
    public List<Book> findAll() {
        List<String> data = readFile(FILE_NAME);

        List<Book> books = new ArrayList<>();
        data.forEach(line -> books.add(createBook(line)));

        return books;
    }

    @Override
    public void save(Book book) {

    }

    @Override
    public List<Book> findByAuthor(Author author) {
        return null;
    }

    @Override
    public List<Book> findByName(String name) {
        return null;
    }

    private Book createBook(String line) {
        String[] parts = line.split("\\|");
        Author author = new Author(parts[0]);
        String name = parts[1];
        Integer issueYear = 0;

        try {
            issueYear  = Integer.parseInt(parts[2]);
        } catch(NumberFormatException e) {
            System.err.println(e);
        }

        return new Book(author, name, issueYear);
    }
}
