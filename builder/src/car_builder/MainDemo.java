package car_builder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class MainDemo {
    public static final String FILE_NAME = "builder\\src\\OutputDemo.txt";

    public static void main(String[] args) {
        AutomationEngineer automationEngineer = new AutomationEngineer();

        // Инженер по автоматизации получает объект конкретного строителя от клиента
        // (приложения). Приложение само знает какой строитель использовать,
        // чтобы получить нужный продукт.
        CarBuilder builder = new CarBuilder();
        automationEngineer.constructSportsCar(builder);

        // Готовый продукт возвращает строитель, так как Инженер по автоматизации чаще всего не
        // знает и не зависит от конкретных классов строителей и продуктов.
        Car car = builder.getResult();
        save("Продукт машина сформирован:\n" + car.getCarType());

        CarManualBuilder manualBuilder = new CarManualBuilder();

        // Инженер по автоматизации может знать больше одного рецепта строительства.
        automationEngineer.constructSportsCar(manualBuilder);
        Manual carManual = manualBuilder.getResult();
        save("\nРуководство пользователя по продукту Машина сформировано:\n" + carManual.print());

        System.exit(0);
    }

    public static void save(String text) {
        // Пример try with resources
        try (
                Writer writer = new FileWriter(FILE_NAME, true);
                BufferedWriter bufferedWriter = new BufferedWriter(writer);
            ){
            bufferedWriter.write(text); // запишем текст
            bufferedWriter.newLine(); // перейдем на другую строку, аналог + "\n"
            bufferedWriter.flush(); // обновим данные при записи в runtime, необязательно
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
