package stream;

public enum Level {
    TRAINEE,
    JUNIOR,
    MIDDLE,
    SENIOR
}
